package edu.uchicago.gerber.prohello;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

     //2 define members of views defiened in the layout
    private TextView textHello;
    private Button buttonGo;
    private EditText editHello;
    private View viewBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //3 inflate
        setContentView(R.layout.activity_main);

        //4 get referenes to the objects on the heap
        textHello = (TextView) findViewById(R.id.textHello);
        editHello = (EditText) findViewById(R.id.editHello);
        buttonGo = (Button) findViewById(R.id.buttonGo);
        viewBar = findViewById(R.id.viewBar);

        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strHelloThere = "Hello there: ";
                strHelloThere += editHello.getText().toString();
                textHello.setText(strHelloThere);
                Random rnd = new Random();
                int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                viewBar.setBackgroundColor(color);



            }
        });




    }
}
